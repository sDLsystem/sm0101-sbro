 --- ma{NN}F: **Model application {NN} framework** (inFiles: sbroFMK-ma{NN}F.yml, class: DMAIC/project)

1. [exec sys_install/sys] (Re)create needed tables

#. [exec ma{NN}F_main] Validate the existence of dataout f{NN}F tables

#. [...] Generate prediction microservices

#. [...] Activate prediction microservices

#. [run] exec pF_main
