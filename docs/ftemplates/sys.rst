--- sys: **Base system framework** (inFiles: sbroETL-sys.yml, class: FOSM/project) -- mandatory

1. [exec sys_install/M01] (Re)create default system tables

#. [...] (Re)create datain table "DinINST" [institutional format]

#. [...] (Re)reate dataout table "DoutINST" [institutional format]

#. [...] (Re)create datain table "DinSDL" [CDF/sys format]

#. [...] (Re)create dataout table "DoutSDL" [CDF/sys format]

#. [...] Create tables of other frameworks (if needed)

#. [exec sys_config/M01] Set system configurations

#. [exec sys_docs/M02] Generate Product-UseCase documentation

#. [up] Load SDL specific services, sbroETL variants "{*}F|{b,k}D{in,out}G" services and sbroBRT services

#. [down] Unload all services create by up

#. [exec sys_main/M01] Execution of Product-UseCase maintenance tasks and management of services

#. [run] exec sys_docs
