D03-MissionPurpose
==================

- **Description**: Develop relevant technologies that make workplaces in the industry more attractive; Optimize the definition of work tasks in order to minimize or eliminate health problems, improving ergonomics; Develop technology for the new digitalization paradigm; Use smart tools to measure and promote employee performance, motivation and appreciation; Take advantage of cyber-physical systems integration and ultra-connectivity in the industrial value chain.

