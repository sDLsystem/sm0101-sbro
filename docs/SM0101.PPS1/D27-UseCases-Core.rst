D27-UseCases-Core
=================

------------

UC01
~~~~

------------

.. [UC01] **SM0101.PPS1.UC01 - Data analystics and visualization of ?**

- **aims**: TBD

- **copromotors**: [SM]_ [UA]_

- **hardwareAdded**: TBD

- **location**: SM ??

- **state**: Dashboarding design

- **teams**:

 - **T00**: [SM.CB]_ [UA.AB]_ [UA.ER]_

 - **T01**: [UA.ER]_

 - **T02**: [UA.AB]_

- **product**: [SM0101.PPS1.W1.DADF]_

- **current_attained_results**: TBD

