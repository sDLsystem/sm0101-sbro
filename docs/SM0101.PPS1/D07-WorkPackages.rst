D07-WorkPackages
================

------------

SM0101.PPS1.W1
~~~~~~~~~~~~~~

------------

.. [SM0101.PPS1.W1] **Consultancy and support in the modernization of the company's information system, in the context of managing CAD data of mold models**

- **D09-CriticalSuccessFactors**: TBD

- **D10-Partners**: [SMSA]_ [UA]_

- **D11-ENESII**: [UA]_

- **D12-EstimatedRisks**: Connection problem with company's datalake

- **D13-challengesScientific**: None

- **D14-challengesCommercial**: Custom made

- **D15-InnovativeCharacteristics**: [IC01]_ [IC02]_

- **D17-Frameworks**: [DADF]_

------------

SM0101.PPS1.W2
~~~~~~~~~~~~~~

------------

.. [SM0101.PPS1.W2] **Study and prototyping of a machine learning / deep learning model for image search by similarity**

- **D09-CriticalSuccessFactors**: Quality of the information

- **D10-Partners**: [SMMDA]_ [UA]_

- **D11-ENESII**: [UA]_

- **D12-EstimatedRisks**: Some classes are too much inbalanced.

- **D13-challengesScientific**: Novalty in the approach.

- **D14-challengesCommercial**: Reasonable challenges.

- **D15-InnovativeCharacteristics**: [IC02]_

- **D17-Frameworks**: [ISSF]_

------------

SM0101.PPS1.W3
~~~~~~~~~~~~~~

------------

.. [SM0101.PPS1.W3] **Development of a web application for information search based on image similarity**

- **D09-CriticalSuccessFactors**: TBD

- **D10-Partners**: [SMIMA]_ [UA]_

- **D11-ENESII**: [UA]_

- **D12-EstimatedRisks**: TBD

- **D13-challengesScientific**: TBD

- **D14-challengesCommercial**: TBD

- **D15-InnovativeCharacteristics**: [IC02]_

- **D17-Frameworks**: [ISSF]_

------------

SM0101.PPS1.W4
~~~~~~~~~~~~~~

------------

.. [SM0101.PPS1.W4] **Mobile application development and 30h consultancy/training on topics related to Big Data and Industry 4.0.**

- **D09-CriticalSuccessFactors**: Well adapted training program

- **D10-Partners**: [SMIMG]_ [UA]_

- **D11-ENESII**: [UA]_

- **D12-EstimatedRisks**: Participants heterogeneous knowledge.

- **D13-challengesScientific**: TBD

- **D14-challengesCommercial**: Custom

- **D15-InnovativeCharacteristics**: [IC02]_ [IC03]_

