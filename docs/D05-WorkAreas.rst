D05-WorkAreas
=============

------------

SM0101.PPS0
~~~~~~~~~~~

------------

.. [SM0101.PPS0] **Management, dissemination and demonstrator**

- **coordination**: [SM]_ [UA]_

- **shortDescription**: PPS0 deals with the management tasks of the project.

------------

SM0101.PPS1
~~~~~~~~~~~

------------

.. [SM0101.PPS1] **Core WorkAreas**

- **coordination**: [UA]_

- **shortDescription**: Proposes to create interrelated and integrated solutions for problems, currently faced by the industry, related to predictive maintenance, data-driven asset/plant performance optimization, and data-driven quality control by taking advantage of mathematical modelling and recent machine learning techniques that have been proven to be effective in various sectors with impact on productivity and reduction of operational costs.

- **mainHumanResources**:

 - **Grupo Simoldes**:

  - **members_core**:

  .. [SM.CB] Carlos Barbosa

  .. [SM.JM] João Moreira [contactPoint]

 - **Universidade de Aveiro**:

  - **members_core**:

  .. [UA.ER] Eugénio Rocha [contactPoint]

  .. [UA.AB] Ângela Brochado

